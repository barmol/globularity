# This repository contains the code used for analysis and visualization published in

> Molz, B., Eising, E., Alagöz, G., Schijven, D., Francks, C., Gunz, P., Fisher, S.E. (2024). Imaging Genomics reveals genetic architecture of the globular human braincase



## Repositor content 
## Phenotypic Analysis 
### Prep Phenotype
1. Derive inverse affine transformationmatrices from two data sets
2. Generate Globularity scores, code by Philipp Gunz, Fig 1
3. Phenotype QC
### Phenotypic association
Phenotypic assessment of the trait, Script for Fig S1

## Genetic Analysis
### PreGWAS
- Subsetting of the UKBB data as described in the Method section under Sample QC
- Variant QC as described in the Method section
- *Main scripts prepared by Dick Schijven*
### GWAS
- Scripts for running the genome wide association analysis with Plink
- Merging of derived output
- Manhattan and QQ plot script for Figure 2A and S2
### LDSC
Overall code to munge globularity sumstats and retrieve heritability estimates 
#### UKBB brain imaging
- Code for downloading and formatting summary statistics from [UK Biobank Big40 server](https://open.win.ox.ac.uk/ukbiobank/big40/)
- Code for munging UKB sumstats and running LDSC with globularity sumstats
- Code for creating Figure 2B
#### Complex traits
- Code for running LDSC with traits retrieved via differen resources (see Methods)
- Code for creating Figure 3
#### Partitioned Heritability - Chromatine
- Code for Cell-type-specific-analyses using the Multi Tissue Chromatine data sets
- Code for Plotting Fig4C in Functional Analysis
##### Partitoned Heritability - Evo
- Code for Evolutionary analysis by Gökberk Alagöz
#### Functional Analysis
- Code for plotting Figure 4 and Figure S6; 
- Input data retrieved from after running [FUMA] (https://fuma.ctglab.nl/) (see Methods for Details)
#### Supplements
Contains code for all Supplementary Figures / analysis not yet described 
		

