#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 16:48:18 2021

@author: barmol
"""

import pandas as pd
import matplotlib.pyplot as plt

#-------------------------------------------
#Load list of IDs that survived genetic QC - sample white european
gen_filter_we = pd.read_csv ('/data/workspaces/lag/workspaces/lg-ukbiobank/derived_data/genetic_data/snp/subset_imagingT1_40k/v2_white_ancestry/SQC/imagingT1_wa_ind_list_sqc_postrelatedness.txt',header = None, names=['eid'])
#Load ID's of current sample
eid = pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/primary_data/current_release/ukb43760/ukb43760.csv', sep =",", usecols =[0])
mask_current_release = eid['eid'].isin(gen_filter_we['eid'])

#--------------------------------------------------------------------
#Load list of particpants excluded after phenotype QC
pheno_qc=pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/QC/outlier_after_qc.txt',sep= "\t",header=None)
pheno_qc.columns=['eid','glob','issue']
mask_pheno_qc = ~eid['eid'].isin(pheno_qc['eid'])
#---------------------------------------------------------------------
#also load the list of unusable T1's and fitler by these as the freesurfer output is likely corrupted
unusable_t1 = pd.read_fwf ('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/enigma_evol/UKBB/unusable_T1/noT1_log.txt', header = None)
unusable_t1.columns =['eid']
mask_unusable = ~eid['eid'].isin(unusable_t1['eid'])
#---------------------------------------------------------------------

# now get overal maske and EID of current sample
globularity_sample = pd.concat([mask_current_release, mask_unusable, mask_pheno_qc], axis =1)
globularity_sample = globularity_sample.all(axis =1)
eid_glob_sample = eid[globularity_sample]
globularity_sample.to_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/mask_globsample_ukb43760.dat', index=False, header=False)

#save 
globularity_sample.to_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/mask_globularity_ukb43760.dat', index = False, header = False)
eid_glob_sample .to_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/eid_globularity_ukb43760dat',header =False, index = False)


#Load list with globularity scores
glob=pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/philipp/2020-globularityscores-Beta-B.txt',sep= "\t")
glob_mask=glob['ID'].isin(eid_glob_sample['eid'])
glob_sample=glob[glob_mask]
glob_sample=glob_sample.set_index('ID')
glob_sample.to_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/pheno_globularity_ukb43760.dat',header=None)






#latest input to get a full demographics overview: importat covariates list and exlcuded genetic covars
# get the inital covariate file prepared for the GWAS
covar=pd.read_excel('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/covariats_glob_ukb43760.xlsx')
# get the IDs with diagnosis term
eid_diagnosed = pd.read_csv ('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/enigma_evol/UKBB/ICD_diagnosis/ukb43760/eid_neuro_diagnosed_ukb43760.dat')
# get a boolean mask of people diagnosed in our sample 
mask_diagnosed = glob['eid'].isin(eid_diagnosed['eid'])
# load additonal values needed --> eTIV and T1 scaling factor
ukb_values =pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/primary_data/current_release/ukb43760/ukb43760.csv', sep =",", usecols =['eid','26521-2.0','25000-2.0'])
# load our sample mask to filter the above UKBB values
globularity_sample=pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/pheno_prep/mask_globularity_ukb43760.dat', header =None)
ukb_values_sample=ukb_values[globularity_sample[0]]
# rename the columns
ukb_values_sample.columns=['eid','scaling','etiv']
# concat  globularity scores with health info
health=pd.concat([glob_sample,mask_diagnosed],axis=1)
# get decent column names
health.columns=['eid','glob','diagnosis']
# replace boolean info with meaningful values
health['diagnosis'] = health['diagnosis'].replace({True: 'Disorder', False: 'Healthy'})
# make final dataframe combining globscroes,health data, old and new covariates
ukb_glob_demographics=pd.concat([health.set_index['eid',inplace=True],covar.set_index['eid',inplace=True],ukb_values_sample.set_index['eid',inplace=True]],axis=1)
# get rid of genetic PCs's
ukb_glob_demographics.drop(ukb_glob_demographics.iloc[:,4:14],axis =1, inplace=True)
# get rid of other details (batch and interaction terms)
ukb_glob_demographics.drop(ukb_glob_demographics.iloc[:,9:13],axis =1, inplace=True)
# fromat etiv
ukb_glob_demographics.etiv = ukb_glob_demographics.etiv.map(lambda x: '{:.2E}'.format(x))
# save demographics info
ukb_glob_demographics.to_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/ukb43760_glob_demographics.csv')
#---------------------------------------------------------------------
# # Let's plot
# #age vs globularity
# ax=sns.jointplot(x="age_at_scan", y="glob",kind='reg', data=ukb_glob_demographics,height=8, ratio=5,color='darkcyan',joint_kws={'line_kws':{'color':'limegreen'}});
# # JointGrid has a convenience function
# ax.set_axis_labels('Age', 'Globularity score', fontsize=12, fontweight ='bold')
# ax.ax_joint.tick_params(axis='both', which='major', labelsize=12)
# ax.ax_joint.tick_params(axis='both', which='minor', labelsize=12)
# # labels appear outside of plot area, so auto-adjust
# plt.tight_layout()
# ax.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/ukbb_ageVSglob.png',bbox_inches="tight")


# #etiv - need to hack labels here as otherwise it's non scientific annothations and numbers huge
# labels = ['0.75e$^{+06}$', '1.0e$^{+06}$', '1.25e$^{+06}$','1.5e$^{+06}$','1.75e$^{+06}$','2.0e$^{+06}$','2.25e$^{+06}$']
# ax=sns.jointplot(x="etiv", y="glob",kind='reg', data=ukb_glob_demographics,height=8, ratio=5,color='darkcyan',joint_kws={'line_kws':{'color':'limegreen'}});
# # JointGrid has a convenience function
# ax.ax_joint.set_xticks((750000,1000000,1250000,1500000,1750000,2000000,2250000))
# ax.ax_joint.set_xticklabels(labels, rotation='horizontal')
# ax.set_axis_labels('eTIV $(mm^3)$', 'Globularity score', fontsize=12, fontweight ='bold')
# #ax.ax_joint.tick_params(axis='both', which='major', labelsize=12)
# #ax.ax_joint.tick_params(axis='both', which='minor', labelsize=12)
# # labels appear outside of plot area, so auto-adjust
# plt.tight_layout()
# ax.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/ukbb_etivVSglob.png',bbox_inches="tight")


# #not saved
# ax=sns.jointplot(x="scaling", y="glob",kind='reg', data=ukb_glob_demographics,height=8, ratio=3,color='darkcyan',joint_kws={'line_kws':{'color':'limegreen'}});
# # JointGrid has a convenience function
# ax.set_axis_labels('Age', 'Globularity score', fontsize=12, fontweight ='bold')
# ax.ax_joint.tick_params(axis='both', which='major', labelsize=12)
# ax.ax_joint.tick_params(axis='both', which='minor', labelsize=12)
# # labels appear outside of plot area, so auto-adjust
# plt.tight_layout()


# #globularity vs neurological problems
# fig, ax = plt.subplots(figsize=(8,8)) 
# ax = pt.RainCloud(x = 'diagnosis', y = 'glob', 
                  # data = ukb_glob_demographics, 
                  # width_viol = .4,
                  # width_box = .3,
                  # orient = 'v',
                  # move = .0,
                  # box_showfliers=False,
                  # point_size = 5,
                  # palette = 'viridis',
                  # pointplot = True)
# ax.spines['right'].set_visible(False)
# ax.spines['top'].set_visible(False)
# ax.spines['left'].set_visible(True)
# ax.axes.yaxis.set_visible(True)
# ax.set_xlabel('Health status',fontsize=12, fontweight ='bold')
# ax.set_ylabel('Globularity score',fontsize=12, fontweight ='bold')
# plt.tight_layout()
# _, xlabels = plt.xticks()
# ax.set_xticklabels(xlabels,size = 14)
# fig.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/ukbb_diagnosisVSglob.png',bbox_inches="tight")

