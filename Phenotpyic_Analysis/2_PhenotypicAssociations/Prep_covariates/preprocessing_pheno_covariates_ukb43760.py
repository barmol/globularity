#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 22 13:37:36 2021

@author: barmol
"""

'''
here we import the confounds that are important for the current study,
intersect the confounds list with the current sample, and create additional covariats e.g. age x sex interactions
--> output: confound excel file for globularity

'''
import pandas as pd 
import numpy as np
#--------------------
eid = pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/primary_data/current_release/ukb43760/ukb43760.csv', sep =",", usecols =[0])
mask_globularity=pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/mask_globsample_ukb43760.dat', header=None)
confound_values =pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/primary_data/current_release/ukb43760/ukb43760.csv', sep =",", usecols =['31-0.0','21003-2.0','22009-0.1','22009-0.2','22009-0.3','22009-0.4','22009-0.5','22009-0.6','22009-0.7','22009-0.8', '22009-0.9', '22009-0.10', '25756-2.0', '25757-2.0', '25758-2.0', '25734-2.0','25735-2.0','54-2.0','22000-0.0'])
# sort the columns again as pandas rearranges them... 
confound_values = confound_values[['31-0.0','21003-2.0','22009-0.1','22009-0.2','22009-0.3','22009-0.4','22009-0.5','22009-0.6','22009-0.7','22009-0.8', '22009-0.9', '22009-0.10', '25756-2.0', '25757-2.0', '25758-2.0', '25734-2.0','25735-2.0','54-2.0','22000-0.0']]
confound_values.columns = ['sex','age_at_scan','PC1','PC2','PC3','PC4','PC5', 'PC6','PC7', 'PC8','PC09','PC10','xposition','yposition','zposition','t1SNR','t1CNR','assessmentcentre','genotypearray']
#as we are manipulating the dataframe, make a copy
#as we are manipulating the dataframe, make a copy
confound_sample = confound_values[mask_globularity[0]].copy()
eid_sample=eid[mask_globularity[0]].copy()
#change sex value from 0 female 1 male to female = 2 male =1 
confound_sample.loc[confound_sample['sex'] == 0, 'sex'] = 2
confound_sample.loc[confound_sample['genotypearray'] < 0, 'genotypearray'] = 0
confound_sample.loc[confound_sample['genotypearray'] > 0, 'genotypearray'] = 1

# get mean age in sample
age_mean =confound_sample['age_at_scan'].mean()
# get mean centred age
confound_sample.loc[:,'ageCSq'] = confound_sample.loc[:,'age_at_scan'].apply(lambda x: np.square(x - age_mean))
# get sex age interaction
confound_sample.loc[:,'age_sex'] = confound_sample.loc[:,'age_at_scan'] * confound_sample.loc[:,'sex']
#get sex mean centred age interaction
confound_sample.loc[:,'ageCSq_sex'] = confound_sample.loc[:,'ageCSq'] * confound_sample.loc[:,'sex']

#let's factorize the categorical variables 
cat_variables=['sex','assessmentcentre','genotypearray']
for col in cat_variables:
    confound_sample[col] = pd.factorize(confound_sample[col])[0]
confound_sample=pd.get_dummies(confound_sample, columns=['assessmentcentre'],drop_first =True)
#concatenate with EID
confound_sample_final=pd.concat([eid_sample,confound_sample],axis=1) 
#save it
confound_sample_final.to_excel('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/covariats_glob_ukb43760.xlsx',index = None)

