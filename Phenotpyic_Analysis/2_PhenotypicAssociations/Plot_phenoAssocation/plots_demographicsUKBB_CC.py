#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 15 17:23:02 2022
This script will take globularity scores and demographic information of both data sets and plot relationships:
Age, healt status, eTIV and scaling factor are the main metrics of interest.
At the end we do some additonal QC / plotting and check the influence of outliers in volume/scaling on globularity scores. 

@author: Barbara Molz
"""
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import ptitprince as pt
from statsmodels import robust
from scipy import stats


#import the two demographic files
cC_glob_demographics=pd.read_csv('/data/workspaces/lag/workspaces/lg-globularity/analysis/camCan_globularity_demographics.csv')
ukb_glob_demographics=pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/ukb43760_glob_demographics.csv')

#get standard  overview of NANS in the sample
missingDataUKBB=ukb_glob_demographics.isna().sum()
missingDataCamCam=cC_glob_demographics.isna().sum()
#----------------------------------------------------------------
#First do some standard overview plots
#----------------------------------------------------------------

#1.1 AGE - CamCan


g = sns.jointplot(data=ukb_glob_demographics, x=age_at_scan, y=glob, hue=Sex)

for _,gr in penguins.groupby(Sex):
    sns.regplot(x=age_at_scan, y=glob, data=gr, scatter=False, ax=g.ax_joint, truncate=False)


#1.1. AGE - UKBB
plt.figure(figsize=(6,6))
ax=sns.jointplot(x="age_at_scan", y="glob",kind='reg', data=ukb_glob_demographics,height=8, ratio=5,color='darkcyan',joint_kws={'line_kws':{'color':'darkslategrey'}});
# JointGrid has a convenience function
ax.set_axis_labels('Age', 'Globularity score', fontsize=12, fontweight ='bold')
ax.ax_joint.tick_params(axis='both', which='major', labelsize=12)
ax.ax_joint.tick_params(axis='both', which='minor', labelsize=12)
# labels appear outside of plot area, so auto-adjust
plt.tight_layout()
ax.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/ukbb_ageVSglob.png',bbox_inches="tight")

#2. ETIV - CamCan

#2.2 ETIV - UKBB
#etiv - need to hack labels here as otherwise it's non scientific annothations and numbers are huge
labels = ['0.75e$^{+06}$', '1.0e$^{+06}$', '1.25e$^{+06}$','1.5e$^{+06}$','1.75e$^{+06}$','2.0e$^{+06}$','2.25e$^{+06}$']
plt.figure(figsize=(6,6))
ax=sns.jointplot(x="etiv", y="glob",kind='reg', data=ukb_glob_demographics,height=8, ratio=5,color='darkcyan',joint_kws={'line_kws':{'color':'darkslategrey'}});
ax.ax_joint.set_xticks((750000,1000000,1250000,1500000,1750000,2000000,2250000))
ax.ax_joint.set_xticklabels(labels, rotation='horizontal')
ax.set_axis_labels('eTIV $(mm^3)$', 'Globularity score', fontsize=12, fontweight ='bold')
ax.set_xticklabels(xlabels,size = 14)
#ax.ax_joint.tick_params(axis='both', which='major', labelsize=12)
#ax.ax_joint.tick_params(axis='both', which='minor', labelsize=12)
plt.tight_layout()
ax.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/ukbb_etivVSglob.png',bbox_inches="tight")


#3. SCALING
plt.figure(figsize=(6,6))
ax=sns.jointplot(x="scaling", y="glob",kind='reg', data=ukb_glob_demographics,height=8, ratio=3,color='darkcyan',joint_kws={'line_kws':{'color':'darkslategrey'}});
ax.set_axis_labels('Age', 'Globularity score', fontsize=12, fontweight ='bold')
ax.ax_joint.tick_params(axis='both', which='major', labelsize=12)
ax.ax_joint.tick_params(axis='both', which='minor', labelsize=12)
ax.set_xticklabels(xlabels,size = 14)
plt.tight_layout()


#4. Globularity vs DISORDER
fig, ax = plt.subplots(figsize=(6,6)) 
ax = pt.RainCloud(x = 'diagnosis', y = 'glob', 
                  data = ukb_glob_demographics, 
                  width_viol = .4,
                  width_box = .3,
                  orient = 'v',
                  move = .0,
                  box_showfliers=False,
                  point_size = 5,
                  palette = 'viridis',
                  pointplot = True)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['left'].set_visible(True)
ax.axes.yaxis.set_visible(True)
ax.set_xlabel('Health status',fontsize=12, fontweight ='bold')
ax.set_ylabel('Globularity score',fontsize=12, fontweight ='bold')
plt.tight_layout()
_, xlabels = plt.xticks()
ax.set_xticklabels(xlabels,size = 12, weight='bold')
ax.set_yticklabels(xlabels,size = 12,weight='bold')
fig.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/ukbb_diagnosisVSglob.png',bbox_inches="tight")



#--------------------------------------------
#Just for sanity checks - let's find ETIV outliers
#--------------------------------------------
ukb_glob_demographics['description']='full Sample'


#QC GLOBAL VALUES by median 5* MAD - need here different function as robust can't deal with NANS
lower = ukb_glob_demographics['etiv'].median() - 5*stats.median_absolute_deviation(ukb_glob_demographics['etiv'],nan_policy='omit')
upper= ukb_glob_demographics['etiv'].median() + 5*stats.median_absolute_deviation(ukb_glob_demographics['etiv'],nan_policy='omit')
#empty dataframe
etiv_values_qc = pd.DataFrame()   
#check each column for outliers, make qc'ed dataframe
etiv_values_qc = ukb_glob_demographics['etiv'].mask((ukb_glob_demographics['etiv'] < lower) | (ukb_glob_demographics['etiv'] > upper))
#simply count outliers - just want a number - compared to NaNs before
etiv_values_qc.isna().sum

#Neat way to visualize the impact of outliers - get the upper and lower 1% 
quantiles = ukb_glob_demographics['etiv'].quantile([.01, .99])

#filter our dataframe for these outside values
filtered_lower = ukb_glob_demographics[ukb_glob_demographics['etiv'] < quantiles.loc[0.01]].copy()
filtered_upper= ukb_glob_demographics[ukb_glob_demographics['etiv'] > quantiles.loc[0.99]].copy()

#let's get a descripton for plotting
filtered_lower['description']='lower 1%'
filtered_upper['description']='upper 1%'

#create new dataframe - I want this ordered from lower range - full data - upper range
frames =[filtered_lower[['glob','description']],ukb_glob_demographics[['glob','description']],filtered_upper[['glob','description']]]
final=pd.concat(frames)
#plot this again
fig, ax = plt.subplots(figsize=(8,8)) 
ax = pt.RainCloud(x = 'description', y = 'glob', 
                  data = final, 
                  width_viol = .4,
                  width_box = .3,
                  orient = 'h',
                  move = .0,
                  box_showfliers=False,
                  point_size = 5,
                  palette = 'viridis')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['left'].set_visible(True)
ax.axes.yaxis.set_visible(True)
ax.set_xlabel('Outlier vs Rest',fontsize=12, fontweight ='bold')
ax.set_ylabel('Globularity score',fontsize=12, fontweight ='bold')
plt.tight_layout()
_, xlabels = plt.xticks()
ax.set_xticklabels(xlabels,size = 12, weight='bold')
ax.set_yticklabels(xlabels,size = 12,weight='bold')
fig.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/plots/ukbb_globscore_etiv_INvsOUT.png',bbox_inches="tight")



#--------------------------------------------
#Just for sanity checks - let's find SCALING outliers
#--------------------------------------------

#QC GLOBAL VALUES by median 5* MAD - will keep function constistent
lower = ukb_glob_demographics['scaling'].median() - 5*stats.median_absolute_deviation(ukb_glob_demographics['scaling'])
upper= ukb_glob_demographics['scaling'].median() + 5*stats.median_absolute_deviation(ukb_glob_demographics['scaling'])
#empty dataframe
scaling_values_qc = pd.DataFrame()   
#check each column for outliers, make qc'ed dataframe
scaling_values_qc = ukb_glob_demographics['scaling'].mask((ukb_glob_demographics['scaling'] < lower) | (ukb_glob_demographics['scaling'] > upper))
#simply count outliers - just want a number - compared to NaNs before
scaling_values_qc.isna().sum

#Easier way to also visualize the impact of outliers - get the upper and lower 1% 
quantiles = ukb_glob_demographics['scaling'].quantile([.01, .99])

#filter our dataframe for these outside values
filtered_lower = ukb_glob_demographics[ukb_glob_demographics['scaling'] < quantiles.loc[0.01]].copy()
filtered_upper= ukb_glob_demographics[ukb_glob_demographics['scaling'] > quantiles.loc[0.99]].copy()

#let's get a descripton for plotting
filtered_lower['description']='lower 1%'
filtered_upper['description']='upper 1%'

#create new dataframe - I want this ordered from lower range - full data - upper range
frames =[filtered_lower[['glob','description']],ukb_glob_demographics[['glob','description']],filtered_upper[['glob','description']]]
final=pd.concat(frames)
#plot this again
fig, ax = plt.subplots(figsize=(8,8)) 
ax = pt.RainCloud(x = 'description', y = 'glob', 
                  data = final, 
                  width_viol = .4,
                  width_box = .3,
                  orient = 'h',
                  move = .0,
                  box_showfliers=False,
                  point_size = 5,
                  palette = 'viridis')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['left'].set_visible(True)
ax.axes.yaxis.set_visible(True)
ax.set_xlabel('Outlier vs Rest',fontsize=12, fontweight ='bold')
ax.set_ylabel('Globularity score',fontsize=12, fontweight ='bold')
plt.tight_layout()
_, xlabels = plt.xticks()
ax.set_xticklabels(xlabels,size = 12, weight='bold')
ax.set_yticklabels(xlabels,size = 12,weight='bold')
fig.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/plots/ukbb_globscore_etiv_INvsOUT.png',bbox_inches="tight")




#--------------------------------------------
#Just for sanity checks - what happens with age?
#--------------------------------------------


#Easier way to also visualize the impact of outliers - get the upper and lower 1% 
quantiles = ukb_glob_demographics['age_at_scan'].quantile([.01, .99])

#filter our dataframe for these outside values
filtered_lower = ukb_glob_demographics[ukb_glob_demographics['age_at_scan'] < quantiles.loc[0.01]].copy()
filtered_upper= ukb_glob_demographics[ukb_glob_demographics['age_at_scan'] > quantiles.loc[0.99]].copy()

#let's get a descripton for plotting
filtered_lower['description']='lower 1%'
filtered_upper['description']='upper 1%'

#create new dataframe - I want this ordered from lower range - full data - upper range
frames =[filtered_lower[['glob','description']],ukb_glob_demographics[['glob','description']],filtered_upper[['glob','description']]]
final=pd.concat(frames)
#plot this again
fig, ax = plt.subplots(figsize=(8,8)) 
ax = pt.RainCloud(x = 'description', y = 'glob', 
                  data = final, 
                  width_viol = .4,
                  width_box = .3,
                  orient = 'h',
                  move = .0,
                  box_showfliers=False,
                  point_size = 5,
                  palette = 'viridis')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['left'].set_visible(True)
ax.axes.yaxis.set_visible(True)
ax.set_xlabel('Outlier vs Rest',fontsize=12, fontweight ='bold')
ax.set_ylabel('Globularity score',fontsize=12, fontweight ='bold')
plt.tight_layout()
_, xlabels = plt.xticks()
ax.set_xticklabels(xlabels,size = 12, weight='bold')
ax.set_yticklabels(xlabels,size = 12,weight='bold')
fig.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/plots/ukbb_globscore_etiv_INvsOUT.png',bbox_inches="tight")




