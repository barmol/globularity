#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 15 12:32:06 2023

@author: barmol
"""
#import stuff
import seaborn as sns
import matplotlib.pyplot as plt
import ptitprince as pt
import pandas as pd
import matplotlib

#settings for PDF --> Illustrator compability 
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.rcParams['svg.fonttype'] = 'none'


#import the two demographic files
cC_glob_demographics=pd.read_csv('/data/workspaces/lag/workspaces/lg-globularity/analysis/camCan_globularity_demographics.csv')
ukb_glob_demographics=pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/ukb43760_glob_demographics.csv')

#----------------------------------------------------------------
#Phenotypic overview plots
#----------------------------------------------------------------


#================================================================
#CamCAN AGE vs GLOB; Sex stratified 
#===============================================================


f=sns.lmplot(x = "Age", y = "globularity",
           hue = "Sex", data = cC_glob_demographics,palette=['lightsteelblue','darkblue'],scatter_kws={"s": 10, "alpha": 1})
sns.despine(right = True)

f.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/plots/camCan_AgeSex.pdf',bbox_inches="tight")

#=================================================================
# SEX stratified UKBB
#=================================================================

#fake x variable
ukb_glob_demographics['xvar']='fakeX'
fig, ax = plt.subplots(figsize=(6,6)) 
ax = sns.violinplot(x='xvar', y="glob", hue="sex", data=ukb_glob_demographics, palette=['lightsteelblue','darkblue'],inner_kws=dict(box_width=15, whis_width=2, color=".8"))

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['left'].set_visible(True)
ax.axes.yaxis.set_visible(True)
#ax.set(ylabel=None)
ax.set_yticklabels(ax.get_yticks(), weight='normal')
ax.set_xticklabels(ax.get_xticks(), weight='normal')

#ax.scatter(ukb_glob_demographics['diagnosis'], ukb_glob_demographics['glob'],rasterized =True)
ax.set_xlabel('Globularity score',fontsize=12, fontweight ='normal')
fig.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/plots/ukbb_sexStratifiedUpdate.pdf', bbox_inches="tight")


#================================================================
# UKBB ETIV
#================================================================
plt.rcParams["font.weight"] = "bold"
plt.rcParams["axes.labelweight"] = "bold"
labels = ['0.75e$^{6}$', '1.0e$^{6}$', '1.25e$^{6}$','1.5e$^{6}$','1.75e$^{6}$','2.0e$^{6}$','2.25e$^{6}$']
ax=sns.jointplot(x="etiv", y="glob",kind='hex', data=ukb_glob_demographics,height=6, ratio=5,color='steelblue');
#ax.ax_joint.scatter(ukb_glob_demographics['etiv'],ukb_glob_demographics['glob'],rasterized=True)
sns.regplot(x="etiv", y="glob", ax=ax.ax_joint, scatter=False,data=ukb_glob_demographics)
ax.ax_joint.set_xticks((750000,1000000,1250000,1500000,1750000,2000000,2250000))
ax.ax_joint.set_xticklabels(labels, rotation='horizontal')
ax.set_axis_labels('eTIV $(mm^3)$', 'Globularity score', fontsize=12, fontweight ='bold')
cbar_ax =ax.fig.add_axes([.75, .15, .02, .2])  # x, y, width, height
plt.colorbar(cax=cbar_ax)
plt.tight_layout()
ax.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/plots/ukbb_etivVSglob2_hexbin.pdf',bbox_inches="tight")


#================================================================
#UKB Diagnosis 
#================================================================


colors = ["steelblue", "skyblue"]
plt.rcParams["axes.labelweight"] = "bold"
fig, ax = plt.subplots(figsize=(6,6)) 
ax = pt.RainCloud(y = 'glob', x = 'diagnosis', 
                  data = ukb_glob_demographics, 
                  width_viol = .4,
                  width_box = .3,
                  orient = 'h',
                  move = .0,
                  box_showfliers=False,
                  point_size = 5,
                  pointplot = False,
                  palette=colors,
                  rain_rasterized=True)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.spines['left'].set_visible(True)
ax.axes.yaxis.set_visible(True)
ax.set(ylabel=None)
#ax.scatter(ukb_glob_demographics['diagnosis'], ukb_glob_demographics['glob'],rasterized =True)
ax.set_xlabel('Globularity score',fontsize=12, fontweight ='bold')
fig.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/plots/ukbb_diagnosisVSglob2.pdf',bbox_inches="tight")