#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 15 12:32:06 2023

@author: barmol
"""

import seaborn as sns
import matplotlib.pyplot as plt
import ptitprince as pt
from scipy import stats
import pandas as pd

import matplotlib
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
plt.rcParams['svg.fonttype'] = 'none'
cC_glob_demographics=pd.read_csv('/data/workspaces/lag/workspaces/lg-globularity/analysis/camCan_globularity_demographics.csv')

g=sns.jointplot(data=cC_glob_demographics, x='Age', y='globularity', hue='Sex',palette=['green','blue'])
for _,gr in cC_glob_demographics.groupby('Sex'):
    sns.lmplot(x='Age', y='globularity', data=gr, scatter=False, ax=g.ax_joint, truncate=False, palette=['green','blue'])
g=sns.lmplot(x='Age',y='globularity', hue='Sex', data=cC_glob_demographics, palette=['green','blue'])


f=sns.lmplot(x = "Age", y = "globularity",
           hue = "Sex", data = cC_glob_demographics,palette=['lightsteelblue','darkblue'],scatter_kws={"s": 10, "alpha": 1})
sns.despine(right = True)

f.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/plots/camCan_AgeSex.pdf',bbox_inches="tight")
