
#!/bin/bash
# this script will performe some standard anatomical preprocessing of camCanT1 files

#get list of all
DATE=`date +"%Y%m%d"`
#-------------------------------------------------------------------------------------
#set up our files: log and error file, the general input path and the outputfolder
logfile=/data/workspaces/lag/workspaces/lg-globularity/analysis/logs/camCan_log2_${DATE}.txt
errorlog=/data/workspaces/lag/workspaces/lg-globularity/analysis/logs/camCan_errorlog2_${DATE}.txt
overfolder=/data/workspaces/lag/workspaces/lg-globularity/primary_data/anat/
outputfolder=/data/workspaces/lag/workspaces/lg-globularity/working_data/transforms
#---------------------------------------------------------------------------------------------------
#redirect errorlogs to file and get date etc
exec 2> $errorlog
date > $logfile 2>&1
date >> $errorlog 2>&1

#-----------------------------------------------------------------------------
#get a list of all subjects
ls $overfolder  > filelist.txt
#loop over subjects
count=0
finalcount=`wc -l filelist.txt`
for filename in `sed -n '10,$ p' filelist.txt`; do
#some logging to know which subject we are currently processing
	echo ${filename}  >> $logfile 2>&1
	echo ${filename}  >> $errorlog 2>&1
	let count++
	printf "Processing participant %s from total %s\n" "${count}" "${finalcount}"
	cd ${overfolder}${filename}/anat/
	T1=${filename}_T1w.nii.gz
	#sanity check - is  there  T1 in the folder? 
	if [ `${FSLDIR}/bin/imtest $T1` = 0 ]; then
		echo No T1 for ${filename}  >> $errorlog 2>&1
		break
	fi
	#inital preproc using fsl anat: fslreorient2std, robustfov, biascorr, inital bet
	fsl_anat -t T1 --clobber --noreg --nononlinreg --noseg --nosubcortseg -i ${T1}
	#proper bet
	cd ${T1%%.*}.anat
	bet T1_biascorr T1_biascorr_bet -R
	# flirt --> get linear transformation matrix T1 --> MNI space
	flirt -ref ${FSLDIR}/data/standard/MNI152_T1_1mm_brain -in T1_biascorr_bet.nii.gz -omat T1_to_MNI_linear.mat.mat
	# Invers it : MNI --> subject space
	convert_xfm -omat ${outputfolder}/${filename#sub-}_inverse.mat -inverse T1_to_MNI_linear.mat.mat
	invers_transform=${outputfolder}/${filename#sub-}_inverse.mat
	if [ ! -f "$invers_transform" ]; then
		echo "$invers_transform does not exist."  >> $errorlog 2>&1
	fi
	# calculate actual transform for potential QC
	#flirt -in $FSLDIR/data/standard/MNI152_T1_1mm_brain -ref T1_biascorr_bet.nii.gz  -out MNI_to_T1_test2.nii.gz -init -init ${outputfolder}/${filename#sub-}_inverse.mat -applyxfm
done
