#!/usr/bin/python

import logging
import os
import numpy as np

outputfolder = '/data/workspaces/lag/workspaces/lg-ukbiobank/analysis/Babs/globularity/transform/'
logging.basicConfig (filename = '/data/workspaces/lag/workspaces/lg-ukbiobank/analysis/Babs/globularity/transform_log.log', level = logging.DEBUG)
# get particpant list
include_list = open('/data/workspaces/lag/workspaces/lg-ukbiobank/working_data/genetic_data/subset_imagingT1_40k/v2_white_ancestry/SQC/imagingT1_wa_ind_list_sqc_postrelatedness.txt', 'r').read().splitlines()
#include_list = open('/data/workspaces/lag/workspaces/lg-ukbiobank/analysis/Babs/globularity/testlist.txt', 'r').read().splitlines()
n_to_process = len(include_list)
os.chdir (outputfolder)
failure_log = []
for value, item in enumerate (include_list,1):
	inputfile = '/data/clusterfs/lag/projects/lg-ukbiobank/primary_data/imaging_data/%s/T1/transforms/T1_to_MNI_linear.mat' %(item)
	os.system('head_top=robustfov -i %s | grep -v Final | head -n 1 | awk {print $5}' %(inputfile))
	os.system('fslmaths %s -roi 0 -1 0 -1 $head_top 170 0 1 T1_tmp' %(inputfile)
	logging.info ('\n processing Nr %s of %s : convert_xfm -omat %s_inverse -inverse %s' %(value, n_to_process,item, inputfile))
	result = os.system('convert_xfm -omat %s_invers  -inverse %s ' % (item, inputfile))
	if 0 != result:
		logging.warning('\n FSL conversion error Nr %s ID %s' %(value, item))
		failure_log.append(item)
#sanitycheck
new_files = os.listdir(os.getcwd()) # dir is your directory path
number_new_files = len(new_files)
print ('\n\n ALL DONE we processed %s transforms from total %s \n\n' %(number_new_files,n_to_process)); 
np.savetxt('/data/workspaces/lag/workspaces/lg-ukbiobank/analysis/Babs/globularity/failure_log.txt', failure_log, fmt ='%s')
