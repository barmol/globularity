#!/bin/sh
#make 2 loops depending on file source


#important paths
camCAN_source=/data/workspaces/lag/workspaces/lg-globularity/primary_data/anat
UKB_source=/data/clusterfs/lag/projects/lg-ukbiobank/primary_data/imaging_data
qc_path=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/QC
file_path=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/test_file.txt
#this was not yet tested inlduced the second check that also last line gets read - as currently the while loop stops before last line
while IFS= read -r line || [ -n "$line" ];; do 
	subj=$(echo "$line" | awk '{print $1}') 
	glob=$(echo "$line" | awk '{print $2}') 
	echo $subj
	outputP=${qc_path}/${subj}_${glob%.*}
	if [[ ${subj} == *"CC"* ]]; then
		inputP=${camCAN_source}/sub-${subj}/anat/sub-${subj}_T1w
		fsleyes render --outfile $outputP --hideCursor --hideLabels --hidex --hidey $inputP
	else
		inputP=${UKB_source}/${subj}/T1/T1
		fsleyes render --outfile $outputP --hideCursor --hideLabels --hidey --hidez $inputP
	fi
done < "$file_path"