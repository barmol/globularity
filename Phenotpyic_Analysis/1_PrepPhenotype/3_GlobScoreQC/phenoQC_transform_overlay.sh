
#important paths
camCAN_source=/data/workspaces/lag/workspaces/lg-globularity/primary_data/anat
UKB_source=/data/clusterfs/lag/projects/lg-ukbiobank/primary_data/imaging_data
qc_path=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/QC
inputfile_path=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/test_file.txt
outputfolderC=/data/workspaces/lag/workspaces/lg-globularity/working_data/transforms
outputfolderU=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/transforms

#this was not yet tested inlduced the second check that also last line gets read - as currently the while loop stops before last line
while IFS= read -r line || [ -n "$line" ]; do 
	subj=$(echo "$line" | awk '{print $1}') 
	glob=$(echo "$line" | awk '{print $2}') 
	echo $subj
	outputP=${qc_path}/overlay_${subj}_${glob%.*}
	if [[ ${subj} == *"CC"* ]]; then
		flirt -in $FSLDIR/data/standard/MNI152_T1_1mm_brain -ref $camCAN_source/sub-${subj}/anat/sub-${subj}_T1w.anat/T1_biascorr_bet.nii.gz  -out ${camCAN_source}/sub-${subj}/anat/${subj}_MNI_to_T1.nii.gz -init ${outputfolderC}/${subj}_inverse.mat -applyxfm
		inputT=${camCAN_source}/sub-${subj}/anat/${subj}_MNI_to_T1
		inputO=$camCAN_source/sub-${subj}/anat/sub-${subj}_T1w.anat/T1_biascorr_bet.nii.gz
		fsleyes render --outfile $outputP --hideCursor --hideLabels $inputO $inputT -cm Red
	else
		flirt -in $FSLDIR/data/standard/MNI152_T1_1mm_brain -ref ${UKB_source}/${subj}/T1/T1_brain -out $outputfolderU/${subj}_MNI_to_T1.nii.gz -init $outputfolderU/${subj}_invers -applyxfm
		inputT=$outputfolderU/${subj}_MNI_to_T1.nii.gz
		inputO=${UKB_source}/${subj}/T1/T1_brain
		fsleyes render --outfile $outputP --hideCursor --hideLabels $inputO $inputT -cm Red
	fi
done < "$inputfile_path"
