#----------------------------------------------------------------------------------------------------------
# Plotting of cell type MAGMA results
# adobpted from Else Eising
# by Barbara Molz
#----------------------------------------------------------------------------------------------------------


#################################################################################################################
## Step 1: Load libraries and list directories
#################################################################################################################

library(stringr)
require(data.table)
library(ggplot2)
library(RColorBrewer)
library(patchwork)




# Define directories, dependent on  system
if (Sys.info()['sysname']=='Windows') {dir="P://workspaces/"} else {dir="/data/workspaces/lag/workspaces/"}

FUMAir = paste(dir, "lg-ukbiobank/projects/globularity/fuma",sep="")

WorkingDir = paste(FUMAir, "/FUMA_celltypePlinkX/", sep="")
GSA <- read.table(paste(WorkingDir,"magma_celltype_step1.txt" , sep="/"), header=TRUE)

significant_GSA = 0.05/length(GSA$Dataset)

GSA$Celltype <- "Other"
GSA$Celltype[grep("Ex", GSA$Cell_type, ignore.case = TRUE)] <- "Excitatory neuron"
GSA$Celltype[grep("RN|Neurons|DA[0-9]|OMTN|Sert", GSA$Cell_type, ignore.case = TRUE)] <- "Neuron"
GSA$Celltype[grep("In|GABA|Gaba", GSA$Cell_type, ignore.case = TRUE)] <- "Inhibitory neuron"
GSA$Celltype[grep("Astro", GSA$Cell_type, ignore.case = TRUE)] <- "Astrocytes"
GSA$Celltype[grep("Micro|Mgl", GSA$Cell_type, ignore.case = TRUE)] <- "Microglia"
GSA$Celltype[grep("Oligo|OPC", GSA$Cell_type, ignore.case = TRUE)] <- "Oligodendrocytes"
GSA$Celltype[grep("Stem_cell|NProg|NbM|IPC", GSA$Cell_type, ignore.case = TRUE)] <- "Neuronal progenitor"
GSA$Celltype[grep("Endo", GSA$Cell_type, ignore.case = TRUE)] <- "Endothelium"


GSA$Dataset_age <- "Adult"
GSA$Dataset_age[grep("GSE104276", GSA$Dataset, ignore.case = TRUE)] <- "Fetal"


GSA <- GSA[order(GSA$P, decreasing=F),]
GSA$Dataset_age <- factor(GSA$Dataset_age, levels=c("Fetal","Adult"))
GSA$Celltype <- factor(GSA$Celltype, levels=c("Neuronal progenitor","Neuron","Excitatory neuron","Inhibitory neuron","Astrocytes","Microglia","Oligodendrocytes","Endothelium","Other"))
GSA$Cell_type <- factor(GSA$Cell_type, levels=GSA$Cell_type[order(GSA$P, decreasing=TRUE)])


GSA_plot <- ggplot(data=GSA, mapping=aes(x=Cell_type, y=-log(P, 10), fill=Celltype)) +
  geom_bar(stat="identity", width=.75) +
  scale_fill_brewer(palette = "Set1") + 
  facet_wrap(~ Dataset_age, scales = "free") +
  scale_y_continuous(limits = c(0,max(-log(GSA$P, 10))), expand = c(0, 0.1)) +
  coord_flip() +
  geom_hline(yintercept=-log(significant_GSA,10), color="grey60", linetype = "longdash") +
  theme_classic() + 
  theme(strip.background = element_blank()) +
  theme(text = element_text(size=10), axis.title = element_text(size = 10), strip.text = element_text(size = 13), axis.text.y = element_text(size = 5, vjust = 0), axis.text.x = element_text(size = 10, vjust = 0.5), legend.text=element_text(size=10)) +
  theme(legend.position = "bottom") +
  labs(x="", y="-log10(Pvalue)")

	
pdf(paste(WorkingDir, "Plot_MAGMA_cell_type_analysis_glob_update.pdf", sep=""), width=8, height=14)			   
GSA_plot
dev.off()