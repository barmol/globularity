import pandas as pd
import csv

#just tiny script to get the snps To keep in the right format for BGENIE
#loop through all snpstats files and get the snps to keep (of the ukBB RS ID column) and save it as white space delimited tab file
file_name = '/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/variant_qc/ukb43760_globularity_chr{}.snpstats_mfi_hrc.snps2keep'
output_name = '/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/snps2keep/ukb43760_globularity_snps2keep_chr{}.table'

df_list = []
for i in range(0, 22):
    df_list.append(pd.read_csv(file_name.format(i+1),sep='\s+',usecols =['RS_ID.UKB']))
    df_list[i].to_csv(output_name.format(i+1), index = None, sep=' ', quoting = csv.QUOTE_NONE)
	
	
	
	
