#!/bin/sh
#$ -N variant_qc
#$ -cwd
#$ -q single.q
#$ -e /data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/log
#$ -o /data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/log
#$ -S /bin/bash
#$ -m eas

chr=$1
pheno=$2


config_file=imaging40k_variant_qc_config_${pheno}.R

printf "RUN VARIANT-LEVEL QC with ${config_file} FOR CHROMOSOME "${chr}"\n\n"
Rscript imaging40k_variant_qc.R ${config_file} ${chr}
printf "\n\n"
