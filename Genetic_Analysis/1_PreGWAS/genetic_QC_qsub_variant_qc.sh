#!/bin/sh
for chrom in {1..21} X
do
	echo "Run variant QC for chrom ${chrom}"
	qsub -M Barbara.Molz@mpi.nl variant_qc_master.sh ${chrom} ukb43760_globularity
done
