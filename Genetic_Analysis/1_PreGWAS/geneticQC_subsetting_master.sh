#!/bin/sh
sourcescript_path=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity
for chrom in {1..21} X
do
	echo "RUN qsub -M Barbara.Molz@mpi.nl -e ${sourcescript_path}/log/ -o ${sourcescript_path}/log/ ${sourcescript_path}/enigma_evo/script/variant_qc/imaging40k_subset_and_snpstats.sh -s ${sourcescript_path}/enigma_evo/script/variant_qc/imaging40k_subset_and_snpstats_config_evol_LeRE_ukb43760.txt -c ${chrom}"	
	qsub -M Barbara.Molz@mpi.nl -e ${sourcescript_path}/log/ -o ${sourcescript_path}/log/ ${sourcescript_path}/script/imaging40k_subset_and_snpstats.sh -s ${sourcescript_path}/script/imaging40k_subset_and_snpstats_config_globularity_ukb43760.txt -c ${chrom}
done 