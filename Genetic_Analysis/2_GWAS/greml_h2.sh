#\!/bin/sh
#$ -N GREML_h2
#$ -o /data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/log
#$ -e /data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/log
#$ -cwd
#$ -q single.q
#$ -S /bin/bash
#$ -M barbara.molz@mpi.nl
#$ -m beas

#------------------------------------------------------------------------
#This script runs GREML heritability --> SEE https://cnsgenomics.com/software/gcta/#GREMLanalysis
#GRM calculated by Dick Schrijven, located at P:\workspaces\lg-ukbiobank\derived_data\genetic_data\snp\subset_imagingT1_40k\v2_white_ancestry\with_rel_filter\grm
# added same covariats as used for running GWAS 
DATE=`date +"%Y%m%d"`
#-------------------------------------------------------------------------------------
#set up working folder
working_dir=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/GREML
output=ukb43760_globularity_with_covar
grm_root=imagingT1_wa_allchr
pheno=pheno_glob_ukb43760_greml.dat
covar_d=glob_covar.txt
covar_q=glob_qcovar.txt

# mkdir -p ${output}
# cd $working_dir
#-------------------------------------------------------------------------------------

# set up pheno and covariate files
#awk -F ',' '{print $1, $1, $2}' pheno_globularity_ukb43760.dat > pheno_glob_ukb43760_greml.dat
#need two different files for continouse and categorigal covars
#awk -F ';' '{print $1, $1, $2, $19, $23, $24}' covariats_glob_ukb43760.csv >glob_covar.txt 
#awk -F ';' '{print $1, $1, $3, $4, $5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17, $18, $20,$21, $22}' covariats_glob_ukb43760.csv > glob_qcovar.txt
#-------------------------------------------------------------------------------------

if [ ! -f ${working_dir}/${output}.hsq ];then
	echo 'One-grm, adjusting for covariates'
	gcta64 --reml --grm ${working_dir}/$grm_root --pheno ${working_dir}/$pheno --covar ${working_dir}/${covar_d} --qcovar ${working_dir}/${covar_q}  --thread-num 10 --out ${working_dir}/${output}  
fi
