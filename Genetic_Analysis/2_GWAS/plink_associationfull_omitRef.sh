#----------------------------------------------------------------------
#!/bin/sh
#$ -N plink_globularity
#$ -q single15.q
#$ -S /bin/bash
#$ -e /data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/log
#$ -o /data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/log
#$ -m eas
#----------------------------------------------------------------------
#load specific PLINK module
module load plink/2.00a
source activate ldsc

#commandline input
chr=$1
model_name=$2
genotype=$3
##--------------------
date +"%D %R"

echo 'Running plink on' ${model_name}' on chromosome' ${chr}

#------------------------------------------------------------------------------------------
# set up stuff e.g. output directory and directory where your bgen files are sitting
bgen_dir=/data/clusterfs/lag/users/barmol/globularity/subsetting
working_dir=/data/clusterfs/lag/users/barmol/globularity/GWAS/plink
bgenie_dir=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity

#create possible missing directories
if [ ! -d ${working_dir} ] && echo "Directory DOES NOT exists - creating it ."; then
	mkdir -p ${working_dir}
fi
cd $working_dir

#here we start a loop which runs PLINK depending on the input - 
#if pheno is male or female we run PLINK with specific covariate file without sex covariates and set the flag to keep males/females only
#otherwise we run the full model, 
#-----------------------------------------------------------------------------------------------------------------------------------------------


if [ ! -f $working_dir/plinkGLM_omitRef_XCI${genotype}_glob_${model_name}_chr${chr}.out.glob.glm.linear ]; then 
	if [[ $model_name == 'female' ]]; then
		echo 'RUN PLINK for CHR ' ${chr} 'on model' ${model_name}
		plink2 \
		--threads 8 \
		--pfile $working_dir/plinkGLM_XCI2_glob_fullSample_chr${chr}.out \
		--extract $bgenie_dir/snps2keep/ukb43760_globularity_snps2keep_chr${chr}.table \
		--keep-females \
		--pheno $bgenie_dir/GWAS/ukb43760_globularity_score_plink.table \
		--covar $bgenie_dir/GWAS/ukb43760_covariates_plinkNoSex_interaction.table \
		--xchr-model ${genotype} \
		--glm no-x-sex hide-covar omit-ref\
		--covar-variance-standardize \
		--vif 85 \
		--out $working_dir/plinkGLM_omitRef_XCI${genotype}_glob_${model_name}_chr${chr}.out
	elif [[ $model_name == 'male' ]];then
		echo 'RUN PLINK for CHR ' ${chr} 'on model' ${model_name}
		plink2 \
		--threads 8 \
		--pfile $working_dir/plinkGLM_XCI2_glob_fullSample_chr${chr}.out \
		--extract $bgenie_dir/snps2keep/ukb43760_globularity_snps2keep_chr${chr}.table \
		--keep-males \
		--pheno $bgenie_dir/GWAS/ukb43760_globularity_score_plink.table \
		--covar $bgenie_dir/GWAS/ukb43760_covariates_plinkNoSex_interaction.table \
		--xchr-model ${genotype} \
		--glm no-x-sex hide-covar omit-ref\
		--covar-variance-standardize \
		--vif 85 \
		--out $working_dir/plinkGLM_omitRef_XCI${genotype}_glob_${model_name}_chr${chr}.out
	elif [[ $model_name == 'full' ]];then
		echo 'RUN PLINK for CHR ' ${chr} 'on model' ${model_name}
		plink2 \
		--threads 8 \
		--pfile $working_dir/plinkGLM_XCI2_glob_fullSample_chr${chr}.out \
		--extract $bgenie_dir/snps2keep/ukb43760_globularity_snps2keep_chr${chr}.table \
		--pheno $bgenie_dir/GWAS/ukb43760_globularity_score_plink.table \
		--covar $bgenie_dir/GWAS/ukb43760_covariates_plinkNoS.table \
		--xchr-model ${genotype} \
		--glm sex hide-covar omit-ref\
		--covar-variance-standardize \
		--vif 85 \
		--out $working_dir/plinkGLM_omitRef_XCI${genotype}_glob_${model_name}_chr${chr}.out
	fi
else
	echo 'OUTPUT FILE ALREADY EXISTST, SKIP'
fi

