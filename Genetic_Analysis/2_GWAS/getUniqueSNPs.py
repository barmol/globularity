#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 10 15:35:25 2022

@author: barmol
"""

import pandas as pd


FumaDir = "/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/fuma/"

femaleSNP=pd.read_csv(FumaDir + "FUMA_plinkOmitRef_female/IndSigSNPs.txt",sep='\t',usecols=['rsID'])
fullSNP=pd.read_csv(FumaDir + "FUMA_plinkOmitRef_full/IndSigSNPs.txt",sep='\t',usecols=['rsID'])
maleSNP=pd.read_csv(FumaDir + "FUMA_plinkOmitRef_male/IndSigSNPs.txt",sep='\t',usecols=['rsID'])
LDSNP=pd.read_csv(FumaDir + "FUMA_plinkOmitRef_full/snps.txt",sep='\t')



overlapAll=list(set(fullSNP.rsID) & set(femaleSNP.rsID) & set(maleSNP.rsID))
overlapF=list(set(fullSNP.rsID) & set(femaleSNP.rsID))
overlapM=list(set(fullSNP.rsID) & set(maleSNP.rsID))

fOnly=femaleSNP[(~femaleSNP.rsID.isin(fullSNP.rsID)) & (~femaleSNP.rsID.isin(maleSNP.rsID))]
mOnly=maleSNP[(~maleSNP.rsID.isin(fullSNP.rsID)) & (~maleSNP.rsID.isin(femaleSNP.rsID))]
test=fOnly.rsID.isin(LDSNP.rsID)
uniqueFemal=fOnly[~test]