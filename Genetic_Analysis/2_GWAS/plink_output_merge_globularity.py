'''
This script loads the output created by BGNENIE, depending on phenotypes (and possible differences in covariates), merges all chromosomes, converts the -log10p value back to normal p values and saves it as a CSV
Input: path to input files, path where output should be saved, phenotypes
Output: CSV folder per pheno with all chromosomes
'''
import pandas as pd
import csv
# create the input and output path variables
file_name ='/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/GWAS/plink/plinkGLM_omitRef_XCI{}_glob_{}_chr{}.out.glob.glm.linear'
#output_name='/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/GWAS/plink/plink_ukb43760_globularity_XCI{}_{}.txt'
output_name='/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/GWAS/plink/plink_omitRef_ukb43760_globularity_XCI{}_{}Autosome.gz'
#output_name='/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/GWAS/plink/plink_omitRef_ukb43760_globularity_XCI{}_{}withX.gz'

#----------------------------
#empty list to store the seperate dataframes (per chromosom)
# get the N for each phenotype
# lets only import what we need as files are already massive
XCImodel=2

for l in ['male','female','full']:
    df_list =[]
    for i in range(0, 22):
        #per overall phenotype, just load what's true for all sub ROIs: Chr, position, rsID, allele 1+2, info, af
        df_list.append(pd.read_csv(file_name.format(XCImodel,l,i+1),sep='\s+'))
    #also get the x-Chromosome 
    #df_list.append(pd.read_csv(file_name.format(XCImodel,l,'X'),sep='\s+'))
    
    df_list = pd.concat(df_list)
    #create the marker
    df_list['marker']=df_list['#CHROM'].astype(str) +':'+ df_list['POS'].astype(str)
    #df_list['#CHROM'].replace(['X'],23)
    df_list.rename(columns={"#CHROM": "CHR"},inplace=True)
    #rename columns
    #save the dataframe and empty the list again as a zipped file using the DK name
    df_list.to_csv(output_name.format(XCImodel,l),index = None,sep=' ', quoting = csv.QUOTE_NONE)
