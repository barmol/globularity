#!/bin/sh
#wrapper script for association analysis with plink_association
# three inputs:
# chrom: current protocol, accepts X
#full sample: specify here if it's all individuals or male/female stratified, and genotype coding for males (model 1 or 2 in plink)
for sample in male female full;do
	for chrom in {1..22} X; do
		echo "RUN qsub -M Barbara.Molz@mpi.nl plink_association.sh ${chrom} ukb43760_globularity for sample ${sample}"
		qsub -M Barbara.Molz@mpi.nl plink_associationfull_omitRef.sh ${chrom} ${sample} 2
	done
done




