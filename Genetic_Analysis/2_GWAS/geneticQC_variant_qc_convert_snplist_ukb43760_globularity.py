import pandas as pd
import csv

#just tiny script to get the snps To keep in the right format for BGENIE
#loop through all snpstats files and get the snps to keep (of the ukBB RS ID column) and save it as white space delimited tab file
file_name = '/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/variant_qc/ukb43760_globularity_chr{}.snpstats_mfi_hrc.snps2keep'
output_name = '/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/snps2keep/ukb43760_globularity_snps2keep_chr{}.table'

df_list = []
for i in range(0, 22):
    i = 'X'
    df_list.append(pd.read_csv(file_name.format(i+1),sep='\s+',usecols =['RS_ID.UKB']))
    df_list[0].to_csv(output_name.format(i), index = None, sep=' ', quoting = csv.QUOTE_NONE)
	
	
sample_blank = pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/subsetting/ukb43760_globularity_chrX.sample',delim_whitespace=True)
    #Update sample files 
file_name = '/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/subsetting/ukb43760_globularity_chr{}.sample'
output_name = '/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/subsetting/update/ukb43760_globularity_chr{}.sample'
for i in range(0, 22):
    test=pd.read_csv(file_name.format(i+1),sep='\s+')
    test2=pd.concat([test, sample_blank['sex']],axis=1)
    test2.to_csv(output_name.format(i+1), index = None, sep=' ', quoting = csv.QUOTE_NONE)
