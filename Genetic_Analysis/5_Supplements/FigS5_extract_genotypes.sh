#!/bin/bash
# this script will extract SNP of interest genotypes from bgen file 


#load necessary modules
module load plink/2.00a

#define paths
bgen_folder=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/subsetting
bgen_file=ukb43760_globularity_chr
chrom=3
output=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity
rs_ID=rs529711487

cd $output
mkdir genotype_SNP

plink2 --bgen ${bgen_folder}/${bgen_file}${chrom}.bgen ref-first --sample ${bgen_folder}/${bgen_file}${chrom}.sample --export A --snp ${rs_ID} --out ${output}/genotype_SNP/${rs_ID}_genotypes




