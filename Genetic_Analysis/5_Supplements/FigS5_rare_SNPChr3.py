#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 25 17:04:55 2022
this script loads globularity score info and the genotype for one SNP
sorts these accordingly, saves carrier file and creates a merged file with both 
genotype and glob score info --> plotted as raincloud plot
@author: barmol
"""
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import ptitprince as pt
plt.rcParams['svg.fonttype'] = 'none'
#load main needed files
#glob scores
glob_score=pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/ukb43760_glob_demographics.csv')
#genotype file
snp_basics=pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/genotype_SNP/genotypes.txt',sep='\t', usecols=['FID','rs529711487_C'])
snp_basics.columns=['eid','genotype']

#thresold dosages
homoAlts=snp_basics[snp_basics.genotype >1.5]
heterozygous=snp_basics[snp_basics.genotype.between(0.5,1.5)]
homoRef=snp_basics[snp_basics.genotype <0.5]

#save the carrieres
heterozygous.to_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/genotype_SNP/heteroz_carrier.txt')

#combine files to contain both genotype and glob score
mergedFile=pd.merge(glob_score[['eid','glob']],snp_basics[['eid','genotype']],on='eid', how='outer')
mergedFile['description']=np.where(mergedFile['genotype']<1.5,'C/T','C/C')
# make raincloud plot
fig, ax = plt.subplots(figsize=(4,6))
ax = pt.RainCloud(x = 'description', y = 'glob', 
                  data = mergedFile, 
                  width_viol = .4,
                  width_box = .4,
                  orient = 'v',
                  move = .0,
                  box_showfliers=False,
                  point_size = 3,
                  palette='Greys',
		  rain_rasterized=True)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.tight_layout()
_, xlabels = plt.xticks()
ax.set_xticklabels(xlabels,size =12)
ax.set_xticklabels(xlabels,size = 12)
fig.tight_layout()
ax.set_xlabel('Genotype',fontsize=12, fontweight ='bold')
fig.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/plots/glob_score_overview_update3.svg',bbox_inches="tight",format='svg')
