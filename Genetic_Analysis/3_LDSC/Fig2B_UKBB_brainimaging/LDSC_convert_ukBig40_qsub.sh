#!/bin/sh
for batch in {1..6} 
do
	echo "convert UKB Big 40 sumstats for batch ${batch}"	
	qsub  LDSC_convert_ukBig40_execute.sh $batch
done 