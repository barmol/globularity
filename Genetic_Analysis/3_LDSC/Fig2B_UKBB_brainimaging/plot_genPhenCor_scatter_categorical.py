#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 17:01:46 2021

@author: barmol
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 17:47:32 2021

@author: barmol
"""
#import stuff
import seaborn as sn
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

#-------------------------------------------------------------------------------
#sort PHENOTYPIC assocation run with PHESANT -- this part is currently not used
#------------------------------------------------------------------------------


#-------------------------------------------------------------------------------
#sort GENETIC correlation run with LDSC
#------------------------------------------------------------------------------
#import txt file containing genetic correlation results
gencor=pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/skew_face_sulcal/gencor_brainimaging/gencor_globularity_imagingTraits.txt',sep='\s+')
# create neat x axis, vaName has gaps so let's so looks odd. Index leads to a mix up of categories
# easiest is to sort var name, then factorise it so we keep the cagetories together, and have a consitent X axis
gencor['trait']=gencor['trait'].str.replace('gencor_glob_','')
# extract additonal measures before adding info
gencor_else=gencor.iloc[630:,:].copy()
gencor_else['ukbID']=np.where(gencor_else['trait'].str.contains('Skew'),2,3)
gencor_else['UKB Imaging category']= np.where(gencor_else['trait'].str.contains('Skew'),'Brain skew','Sulcal morphology')

# We also want to associate the overal imaging category with all genetic correlations so get the info from the overall file and merge
ukb40=pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC_ukb/ukbIDP_imaging_info.csv')
ukbID=ukb40[['UKB ID', 'IDP short name']]
ukbID.columns=['ukbID', 'trait']
all_in = pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/phesant/output/results_combinedAll.txt',sep="\t")
getinfo=all_in[['varName','description','Cat3_Title']].astype(str).copy()
gencor=gencor.merge(ukbID, how='left', on='trait')
#note that phesant data excluded certain traits e.g number of holses before fixing
# the number of traits will go down by 6 after merging
gencor=gencor.merge(getinfo, left_on=  ['ukbID'],
                   right_on= ['varName',], 
                   how = 'inner')
gencor.sort_values(by=['varName'], inplace =True)
gencor.rename(columns={"Cat3_Title" : "UKB Imaging category"},inplace=True)
gencor_update=gencor.drop(columns=['description','varName'])




#concat all dataframes

combinedGenCor=pd.concat([gencor_update,gencor_else])
combinedGenCor_Filterd=combinedGenCor[combinedGenCor['h2_obs']>0.1].copy()

#get overal bonfer. treshold
bonfGen= 0.05/len(combinedGenCor_Filterd)
#make binary significance column for plotting
combinedGenCor_Filterd['bonferoni'] = np.where(combinedGenCor_Filterd['p'] < bonfGen, 'sig','ns.' )
#get simple factorized column for easier plotting of traits
combinedGenCor_Filterd['new'] = pd.factorize(combinedGenCor_Filterd['trait'])[0]
combinedGenCor_Filterd.to_excel('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/skew_face_sulcal/plink_ukb_summary.xlsx',index=None)
#------------------------------------------------------------------------------
#PLOT
#-------------------------------------------------------------------------------
#importfinal data frame again, a metric column was manually added, setting n.s. traits to others, while keeping an overall category for the significant hits only
gencor_processed=pd.read_excel('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/skew_face_sulcal/plink_ukb_summary.xlsx')
#gencor_processed.insert(0, 'New_ID3', range(1, 6670,10))

#set custom color palette 
colors=["midnightblue","gold","orchid","red","green","lightskyblue","orange","grey"] 
sn.set_palette(sn.color_palette(colors))

#plot
# hue is imaging category, size indicates if P value is signifcant
f,ax=plt.subplots(figsize=(12,5))
sn.scatterplot(x='new', y= 'rg', hue='Metric',size='bonferoni',size_order=['sig','ns.'],data=gencor_processed,alpha=0.8, legend ='brief')
plt.legend(bbox_to_anchor=(1.1, 1),borderaxespad=0,frameon=False,labelspacing=0.7)
plt.setp(ax.get_legend().get_texts(), fontsize= '10', weight='bold') # for legend text
plt.setp(ax.get_legend().get_title(), fontsize='10',weight='bold') # for legend title
ax.set_xticks([])
ylabels =np.around(ax.get_yticks(),decimals=2)
ax.set_yticklabels(ylabels, size = 14)
#ax.axhline(y=7.49625187406297e-05,color='grey', linestyle='--')
#ax.axhline(y=-7.49625187406297e-05,color='grey', linestyle='--')
ax.set(ylim=(-0.6, 0.6))
ax.set_xlabel("Brain imaging phenotype",fontsize=14, fontweight='bold',labelpad=15)
ax.set_ylabel('Genetic correlation (rg)',fontsize=14, fontweight ='bold')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.tight_layout()
f.savefig('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/skew_face_sulcal/genoCOR_searborn_Plink.pdf',bbox_inches="tight")



