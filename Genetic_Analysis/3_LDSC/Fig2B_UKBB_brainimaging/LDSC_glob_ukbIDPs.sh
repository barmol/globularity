#\!/bin/sh


#set up the python environment needed on lux13 for LDSC
# module purge
# module load miniconda/3.2021.10 ldsc/v1.0.1
# source activate ldsc

DATE=`date +"%Y%m%d"`
#-------------------------------------------------------------------------------------
#set up our files: the general input path and the outputfolder
#details for munge
ukb_munged=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC_ukb/munge_IDPs
sulcal_munged=/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data/munged_N/
skew_munged=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/skew_face_sulcal/munged_skew
glob_munged=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC/plink/munged_plink_XCI2_fullAutosome.sumstats.gz
out_path=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/skew_face_sulcal/gencor_brainimaging

# details for LDSC
resource_dir=/data/workspaces/lag/shared_spaces/Resource_DB
ldscores_dir=${resource_dir}/LDscores

if [ ! -d ${out_path} ] && echo "Directory DOES NOT exists - creating it ."; then
	mkdir -p ${out_path}
fi
# #-----------------------------------------------------------------------------------------
# #run munge for UKB IDPs

# # for sumstats in ${ukb_munged}/*gz;do
	 # # filename=${sumstats##*/}
	 # # phenotype=${filename%.t*gz} 
	 # # echo 'Run munge_sumstats to create input files for LDSC ' ${phenotype}
	# # python /usr/local/apps/ldsc/munge_sumstats.py \
	 # # --sumstats ${sumstats} \
	 # # --out ${out_path_munge}/munged_${phenotype} \
	 # # --merge-alleles ${ldscores_dir}/w_hm3.snplist
# # done

# # echo 'LDSC munge for UKB IDPs finshed, copying files to workspace'

# # #move files

# # cd $out_path_munge
# # cp * /data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC_ukb/munge_IDPs

# # #set up stuff for gen cor

# #---------------------------------------------------------------------------------------------------
# # run LDSC genetic correlation between UKB IDps and globularity
# for ukbIDP in ${ukb_munged}/*gz;do
	 # filename=${ukbIDP##*/}
	 # phenotype=${filename%.s*gz} 
	 # echo 'Run gen corr between globularity and UKDP: ' ${phenotype}
	# ldsc.py \
	# --rg $glob_munged,$ukbIDP \
	# --ref-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	# --w-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	# --out ${out_path}/gencor_glob_${phenotype#m*_}
# done
# echo 'LDSC gen cor finished for UKBB IDPs'

# #---------------------------------------------------------------------------------------------------
# # run LDSC genetic correlation between suclal morphology and globularity
# for sulcal_file in ${sulcal_munged}/*gz;do
# filename=$(basename "$sulcal_file" .sumstats.gz)
# phenotype=$(echo "$filename" | awk -F '_' '{print $4}' )
	# echo 'Run gen cor between globularity and ' ${phenotype}
	# ldsc.py \
	# --rg $glob_munged,$sulcal_file \
	# --ref-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	# --w-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	# --out ${out_path}/gencor_glob_${phenotype}
# done
# echo 'LDSC gen cor finished for sulcal morphology'


# #---------------------------------------------------------------------------------------------------
# # run LDSC genetic correlation between brain skew and globularity
# for skew_file in ${skew_munged}/*gz;do
# filename=${skew_file##*/}
# phenotype=${filename%.*gz}
	# echo 'Run gen corr between globularity and ' ${phenotype}
	# ldsc.py \
	# --rg $glob_munged,$skew_file \
	# --ref-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	# --w-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	# --out ${out_path}/gencor_glob_${phenotype#m*_}
# done
# echo 'LDSC gen cor finished for brain skew'


#---------------------------------------------------------------------------------------------------
#little wrapper to extract the summary values from the LDSC log after running gen cor
echo "trait rg se z p h2_obs h2_obs_se h2_int h2_int_se gcov_int gcov_int_se"> $out_path/gencor_globularity_imagingTraits.txt
for i in $out_path/*log; do
	res=$(cat $i | grep '^/d.*sumstat' | cut -d" " -f4-)
	if [[ ($i == *'length'*) || ($i == *'surfacearea'*) || ($i == *'width'*) || ($i == *'meandepth'*) ]]; then
		pheno=$(basename "$i" |awk -F '_' '{print $3}')
		echo 'yes'
	else
		pheno=$(basename "$i" | cut -d. -f1)
	fi
	echo $pheno $res >> $out_path/gencor_globularity_imagingTraits.txt
done 

