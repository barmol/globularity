#\!/bin/sh
#$ -N UKB_munge_prep
#$ -o /data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/log
#$ -e /data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/log
#$ -cwd
#$ -q single15.q
#$ -S /bin/bash
#$ -M barbara.molz@mpi.nl
#$ -m beas

batch=$1
printf "RUN the UKBB Big 40 conversion on batch"${batch}"\n\n"

python LDSC_convertSumstats_ukBig40.py ${batch}