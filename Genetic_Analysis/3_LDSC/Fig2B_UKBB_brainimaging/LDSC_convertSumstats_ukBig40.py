#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 14:31:57 2021
This script formats UKBioBank sumstats downloaded from https://open.win.ox.ac.uk/ukbiobank/big40/
It then changes the file naming to more descriptive trait names, converts -logP to P values
includes the correct N per trait and swaps the allle names
UKB : A2 is effect allele
after running this script A1 is effect allele
This makes it easier and is more consistent with other naming conventions.
Afterwards it is saved in the folder defined with ouput name

To increase running spead, input files are devided in batches
@author: barmol
"""
import pandas as pd
import os
import numpy as np
import csv
import sys
batchnumber= sys.argv[1]
#get the ukb info table 
print('current batchNR is', batchnumber)
#this is a table downloaded from the UKB big 40 server with all relevant sumstats details
ukb_info=pd.read_csv('/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/ukbIDP_imaging_info.csv')
#need to format this to allow leading 0 as otherwise indexing wont work
ukb_info['Pheno'] = ukb_info['Pheno'].apply(lambda x: '{0:0>4}'.format(x))

#standard input name per batch
directory='/data/clusterfs/lag/users/barmol/globularity/ukb_sumstats/sumstats_IDPs/batch{}/'
#standard output name with respecitve trait placeholder
outputname='/data/clusterfs/lag/users/barmol/globularity/ukb_sumstats/output/{}.txt.gz'
#go through all the files in our sumstats directory
for count,filename in enumerate(os.listdir(directory.format(batchnumber))):
    #some formatting to get the file ID and use this to extract a more usefull filename
   # get the total number of files
    totalN=len(os.listdir(directory.format(batchnumber)))
    idp1=(os.path.splitext(filename)[0])
    idp2=(os.path.splitext(idp1)[0])
    # print statment to see current inial trait name and the number we are processin atm
    print("{}/{}, current file: {}".format(count+1,totalN,idp1), flush=True)
    #get the trait descrition
    traitname=ukb_info[ukb_info['Pheno']==idp2]['IDP short name'].values[0]
    print('current traitname is', traitname,flush=True)
    #load the actual file
    current_trait =pd.read_csv(os.path.join(directory.format(batchnumber),filename),sep='\s+',dtype={'chr':object,'rsid': object,'pos': np.int64,'a1':object,'a2':object,'beta':np.float64,'se':np.float64,'pval(-log10)':np.float64})
    #convert trait
    current_trait['pval']=10**(-current_trait['pval(-log10)'])
    current_trait.drop(['pval(-log10)'], inplace=True, axis =1)
    # let's get the correct N
    current_trait['N']=ukb_info[ukb_info['Pheno']==idp2]['N(all)'].values[0]
    print ('N for trait', traitname,'is', current_trait['N'][0],flush=True)
    #rename the allele colums - effect allele for UKB BIG40 sumstats is A2 but for LDSC we need this to be A1
    current_trait.rename(columns={'a1': "a2",'a2':'a1'},inplace =True)
    #save it again with a more descriptive name
    current_trait.to_csv(outputname.format(traitname),index = None,sep=' ', quoting = csv.QUOTE_NONE, compression='gzip')
    print( 'file saved',flush=True)
