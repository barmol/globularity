#\!/bin/sh
#set up the python environment needed on lux13 for LDSC
module purge
module load miniconda/3.2021.10 ldsc/v1.0.1
source activate ldsc



DATE=`date +"%Y%m%d"`
#-------------------------------------------------------------------------------------
#set up our files: error log file, the general input path and the outputfolder
project_dir=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/additional_traits/nealLab/
plink_sumstats=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC/plink
resource_dir=/data/workspaces/lag/shared_spaces/Resource_DB
ldscores_dir=${resource_dir}/LDscores


for sumstats in ${project_dir}/new/*;do
	filename=$(basename "$sumstats")
	phenotype=$(echo $filename | awk -F '.' '{print $1}' )
	echo 'run LDSC to calculate genetic correlation between glob and' $phenotype
	ldsc.py \
	--rg ${plink_sumstats}/munged_plink_XCI2_fullAutosome.sumstats.gz,${sumstats} \
	--ref-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	--w-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	--out ${project_dir}/genCor/gencor_ukb43760_glob_${phenotype}
done


# ---------------------------------------------------------------------------------------------------
#extract gen cor resuls 
#----------------------------------------------------------------------------------------------------
#little wrapper to extract the summary values from the LDSC log after running gen cor
echo "trait rg se z p h2_obs h2_obs_se h2_int h2_int_se gcov_int gcov_int_se"> ${project_dir}/genCor/gencor_nealLab.txt
for i in ${project_dir}/genCor/*log; do
	res=$(cat $i | grep '^/d.*sumstat' | cut -d" " -f4-)
	pheno=$(basename "$i" |awk -F '_' '{print $4}')
	echo $pheno $res >> ${project_dir}/genCor/gencor_nealLab.txt
done 