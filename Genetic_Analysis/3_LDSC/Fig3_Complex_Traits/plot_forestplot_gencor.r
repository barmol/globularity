#----------------------------------------------------------------------------------------------------------

library(corrplot)
library(plyr)
library(ggplot2)
library("data.table")
library(grid)
library(viridis)


# Define directories, dependent on  system
if (Sys.info()['sysname']=='Windows') {dir="P://workspaces/"} else {dir="/data/workspaces/lag/workspaces/"}

genCorDir = paste(dir, "lg-ukbiobank/projects/globularity/additional_traits/nealLab/genCor",sep="")

#----------------------------------------------------------------------------------------------------------
# Load data
#----------------------------------------------------------------------------------------------------------
# MTAG results
gencor <- fread(paste(genCorDir,"Book3.txt", sep="/"),header=TRUE)

gencor_sorted=gencor[with(gencor, order(subcategory,trait)),]



significant_genCor = 0.05/length(gencor_sorted$trait)
gencor_sorted$sig=gencor_sorted$p < significant_genCor
#----------------------------------------------------------------------------------------------------------
# Plot data
#----------------------------------------------------------------------------------------------------------

# # Plot correlation with dyslexia and confidence intervals for each trait
# ldhub_plot <- ggplot(gencor_sorted, aes(y = gencor$trait))+

# # Add a point range for MTAG and CP results
  # geom_pointrange(size = 1.5, fatten=2, aes(x = rg, xmin = rg - se, xmax = rg+se, alpha=sig), color = gencor_sorted$subcategory, show.legend = FALSE) +
  
  # # scale_colour_viridis_d() +
  # # scale_color_manual(values=cviridis(n=3)[1:2]) +
  # scale_alpha_discrete(range=c(0.25, 0.8)) +
# # Add a line at zero to divide positive from negative correlations   
# # geom_vline(xintercept = c(-0.5,0.5), linetype="dashed", 
# #             color = "grey", size=0.5) +
  # geom_vline(xintercept = 0,size=0.5) +
  # scale_x_continuous(expand = c(0, 0), limits = c(-0.4,0.4), breaks = c( -0.4, -0.2, 0.0, 0.2, 0.4))+
  # scale_y_discrete(labels=function(x) sub("[1-2]-"," ", x), expand=c(0.3,0)) +
# # Change theme to classic (white background and no gridlines
  # theme_classic()+

  # theme(# Increase thickness of x axis
        # axis.line.x = element_line(colour = "black", size = 1),
		# axis.line.y = element_line(colour = "black", size = 1),
        # # Remove legend
        # legend.position = "none",
		# # Remove ticks from y axis
        # axis.ticks.y = element_blank()) +
		# # specify axis labels
		# labs(x="Genetic correlation (rg)", y="")
# # use other column for y-axis labels


# View plot
ldhub_plot

# store plot
pdf(paste(genCorDir,"new_gencorForestPlot2.pdf",sep='/'), width=7, height=8)   
# Forestplot
forestplot(
df = gencor_sorted,
name = trait,
estimate = rg,
se = se,
logodds = FALSE,
colour = subcategory,
pvalue = p,
psignif = 0.0023,
xlab = "Genetic correlations (rg)"
)

dev.off()

# png(paste(genCorDir,"new_gencorAddon_viridis.png",sep='/'), width=7, height=8, units='in',res=300)   
# # Forestplot
# forestplot(
# df = gencor_sorted,
# name = trait,
# estimate = rg,
# se = se,
# logodds = FALSE,
# colour = subcategory,
# pvalue = p,
# psignif = 0.0023,
# xlab = "Genetic correlations (rg)"
# )

dev.off()
### Legend is added using illustrator