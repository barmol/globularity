#\!/bin/sh

# #set up the python environment needed on lux13 for LDSC
module purge
module load miniconda/3.2021.10 ldsc/v1.0.1
source activate ldsc


DATE=`date +"%Y%m%d"`
# #---------------------------------------------------------------------------------------------------
# #set up our files: the general input path and the outputfolder

project_dir=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity
working_dir=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/additional_traits/psychological/final/
out_path=$working_dir/genCorPlink
glob_munged=$project_dir/LDSC/plink/munged_plink_XCI2_fullAutosome.sumstats.gz
# details for LDSC
resource_dir=/data/workspaces/lag/shared_spaces/Resource_DB
ldscores_dir=${resource_dir}/LDscores


# #-----------------------------------------------------------------------------------------------------


#-----------------------------------------------------------------------------------------------------

if [ ! -d ${out_path} ] && echo "Directory DOES NOT exists - creating it ."; then
	mkdir -p ${out_path}
fi

for munged_file in ${working_dir}/munged/*gz;do
	phenotype=$(basename "$munged_file" .sumstats.gz)
	echo 'Run gen cor between globularity and ' ${phenotype}
	ldsc.py \
	--rg $glob_munged,$munged_file \
	--ref-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	--w-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
	--out ${out_path}/gencor_globularity_${phenotype}
done

echo 'LDSC gen cor finished'
#-----------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
 # extract gen cor resuls 
# #----------------------------------------------------------------------------------------------------
#little wrapper to extract the summary values from the LDSC log after running gen cor
echo "trait rg se z p h2_obs h2_obs_se h2_int h2_int_se gcov_int gcov_int_se"> $out_path/gencor_globularity_psych.txt
for i in $out_path/*log; do
  res=$(cat $i | grep '^/d.*sumstat' | cut -d" " -f4-)
  pheno=$(basename "$i" | awk -F '_' -v OFS=_ '{$1=$2=""; print $0}' )
  echo ${pheno%*.log} $res >> $out_path/gencor_globularity_psych.txt
done 
