#\!/bin/sh
#set up the python environment needed on lux13 for LDSC
module purge
module load miniconda/3.2021.10 ldsc/v1.0.1
source activate ldsc



DATE=`date +"%Y%m%d"`
#-------------------------------------------------------------------------------------
#set up our files: error log file, the general input path and the outputfolder
project_dir=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC
plink_sumstats=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC/plink
resource_dir=/data/workspaces/lag/shared_spaces/Resource_DB
ldscores_dir=${resource_dir}/LDscores


# for sumstats in ${plink_sumstats}/*gz;do
	# filename=$(basename "$sumstats" .gz)
	# phenotype=$(echo $filename | awk -F '_' '{print $5 "_" $6}' )
	# echo 'Run munge_sumstats to create input files for LDSC ' ${phenotype}
	# munge_sumstats.py \
	# --chunksize 500000 \
	# --sumstats ${sumstats} \
	# --snp ID \
	# --a1 A1 \
	# --a2 REF \
	# --N-col OBS_CT \
	# --p P \
	# --out ${plink_sumstats}/munged_plink_${phenotype} \
	# --merge-alleles ${ldscores_dir}/w_hm3.snplist
# done

# ---------------------------------------------------------------------------------------------------
#run genetic correlaton - we have only two so set them up manually 
#----------------------------------------------------------------------------------------------------


# echo 'run LDSC to calculate genetic correlation for autosomes' 
# ldsc.py \
# --rg ${project_dir}/munge_glob_sumstats.gz,${project_dir}/plink/munged_plink_XCI2_fullAutosome.sumstats.gz \
# --ref-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
# --w-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
# --out ${plink_sumstats}/gencor_ukb43760_glob_bgenPlink

echo 'run LDSC to calculate genetic correlation between male/female' 
ldsc.py \
--rg ${project_dir}/plink/munged_plink_XCI2_femaleAutosome.sumstats.gz,${project_dir}/plink/munged_plink_XCI2_maleAutosome.sumstats.gz \
--ref-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
--w-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
--out ${plink_sumstats}/gencor_ukb43760_glob_maleFemale


