#\!/bin/sh
#$ -N LDSC_partitioning_h2
#$ -o /data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/log
#$ -e /data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/log
#$ -cwd
#$ -q single.q
#$ -S /bin/bash
#$ -M barbara.molz@mpi.nl
#$ -m beas



DATE=`date +"%Y%m%d"`
#-------------------------------------------------------------------------------------
#set up our files: error log file, the general input path and the outputfolder
munge_sumstats=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC/munge_globularity.sumstats.gz
out_path=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC
LDSCDir=/data/workspaces/lag/shared_spaces/Resource_DB/ldsc
LDScoreDir=/data/workspaces/lag/shared_spaces/Resource_DB/LDscores/Phase3

h2_dataset=Multi_tissue_chromatin_1000Gv3_ldscores

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#here we run partitioned hertiability, this time only for one annotation but this can be extended 
cd $LDScoreDir
echo 'run partitioned heritability for dataset' ${h2-dataset}
ldsc.py \
--h2-cts $munge_sumstats \
--ref-ld-chr $LDScoreDir/1000G_EUR_Phase3_baseline/baseline.\
--w-ld-chr $LDScoreDir/1000G_Phase3_weights_hm3_no_MHC/weights.hm3_noMHC.\
--print-coefficients \
--out $out_path/parth2_${h2_dataset} \
--ref-ld-chr-cts $LDScoreDir/${h2_dataset}.ldcts
