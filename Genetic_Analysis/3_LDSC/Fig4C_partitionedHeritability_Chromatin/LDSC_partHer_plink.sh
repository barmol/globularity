#\!/bin/sh
#set up the python environment needed on lux13 for LDSC
module purge
module load miniconda/3.2021.10 ldsc/v1.0.1
source activate ldsc

DATE=`date +"%Y%m%d"`
#-------------------------------------------------------------------------------------
#set up our files: the general input path and the outputfolder
glob_munged=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC/plink/munged_plink_XCI2_fullAutosome.sumstats.gz
out_path=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC/plink/parther

# details for LDSC
LDSCDir=/data/workspaces/lag/shared_spaces/Resource_DB/ldsc
LDScoreDir=/data/workspaces/lag/shared_spaces/Resource_DB/LDscores/Phase3

#data set we are running partHeritability on
h2_dataset=Multi_tissue_chromatin

if [ ! -d ${out_path} ] && echo "Directory DOES NOT exists - creating it ."; then
	mkdir -p ${out_path}
fi


#now we run heritability - see details
#https://github.com/bulik/ldsc/wiki/Cell-type-specific-analyses
cd $LDScoreDir
echo 'Run LDSC heritability on dataset' ${h2_dataset}
ldsc.py \
--h2-cts $glob_munged \
--ref-ld-chr $LDScoreDir/1000G_EUR_Phase3_baseline/baseline. \
--w-ld-chr $LDScoreDir/1000G_Phase3_weights_hm3_no_MHC/weights.hm3_noMHC. \
--out $out_path/parth2_${h2_dataset} \
--ref-ld-chr-cts $LDScoreDir/${h2_dataset}.ldcts
