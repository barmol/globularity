#!/bin/sh
#$ -N partherit
#$ -cwd
#$ -q multi15.q
#$ -S /bin/bash

/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/evolution/scripts/globularityevol/partherit/partherit_baseline.sh /data/clusterfs/lag/users/gokala/globularity-evol/data/munged/globularity_plink_autosomes.sumstats.gz neanDepRegions_hg19.sorted /data/clusterfs/lag/users/gokala/globularity-evol/partherit/globularity.neanDepRegions
