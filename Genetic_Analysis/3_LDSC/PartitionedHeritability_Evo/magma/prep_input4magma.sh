#!/bin/bash

# This script will create the necessary input files
# for MAGMA annotation, gene analysis and gene-set
# analysis.

# Gokberk Alagoz, 21.11.22
##########################

# Set Paths

inDir="/data/clusterfs/lag/users/gokala/globularity-evol/data/"
outDir="/data/clusterfs/lag/users/gokala/globularity-evol/magma/input_files/"

##########################

# 1) Prep for magma --annotate

## a) Reformat Genomic SEM meta-analysis results file
## "The SNP location file should contain three columns: SNP ID, chromosome, and base pair position"

awk 'OFS="\t" {print $3, $1, $2}' ${inDir}globularity_plink_full.txt > ${outDir}snp_loc_input.tab

## b) GENELOC_FILE for hg19 is provided in MAGMA website, but you still need to reformat it.
## The gene location file must contain at least four columns, in this order: gene ID, chromosome, start site, stop site.

#awk 'OFS="\t" {print $6, $2, $3, $4}' NCBI37.3.gene.loc > ${outDir}geneLoc_file.tab;

##########################

# 2) Prep for magma Gene analysis
# Reformat globularity sumstats
# "The p-value file must be a plain text data file with each row corresponding to a SNP. If MAGMA detects a header in the file it will look for SNP IDs and p-values in the SNP and P column respectively. Sample size can also be set by using the ncol modifier to specify a column in the p-value file that contains the sample size used per SNP."

awk 'OFS="\t" {print $3, $12, $8}' ${inDir}globularity_plink_full.txt > ${outDir}pval_file_4magma.tab
sed -Ei '1s/ID/SNP/;1s/OBS_CT/N/' ${outDir}pval_file_4magma.tab
