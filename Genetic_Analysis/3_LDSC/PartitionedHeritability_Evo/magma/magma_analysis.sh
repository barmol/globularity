#!/bin/bash

# This script will 1) run snp2gene (annotation),
#		   2) run gene analysis,
#		   3) run gene-set analysis on evo. gene lists.

# Gokberk Alagoz, 11.10.22
##########################

# Set Paths

programs="/home/gokala/programs/"
inDir="/data/clusterfs/lag/users/gokala/globularity-evol/magma/input_files/"
res="/data/workspaces/lag/workspaces/lg-genlang/Working/23andMe/Dyslexia2/Evolution/dys_rhy_pleiotropy/resources/"
outDir="/data/clusterfs/lag/users/gokala/globularity-evol/magma/output_files/"
genotype_f="/data/workspaces/lag/shared_spaces/Resource_DB/magma_v1.10/g1000_eur/"
annot="/data/clusterfs/lag/users/gokala/globularity-evol/magma/input_files/evo_annots.gmt"

# 1) Annotation

${programs}magma --annotate --snp-loc ${inDir}snp_loc_input.tab --gene-loc ${inDir}geneLoc_file.tab --out ${outDir}globularity

# 2) Gene analysis (using SNP p-values)

${programs}magma --bfile ${genotype_f}g1000_eur --pval ${inDir}pval_file_4magma.tab ncol=N --gene-annot ${outDir}globularity.genes.annot --out ${outDir}globularity

# 3) Gene-set analysis

${programs}magma --gene-results ${outDir}globularity.genes.raw --set-annot ${annot} --out ${outDir}evo_annots
