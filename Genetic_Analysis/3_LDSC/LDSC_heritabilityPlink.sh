#\!/bin/sh

# #set up the python environment needed on lux13 for LDSC
module purge
module load miniconda/3.2021.10 ldsc/v1.0.1
source activate ldsc


DATE=`date +"%Y%m%d"`
# #---------------------------------------------------------------------------------------------------
# #set up our files: the general input path and the outputfolder

project_dir=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity
out_path=/data/workspaces/lag/workspaces/lg-ukbiobank/projects/globularity/LDSC/plink
glob_munged=$project_dir/LDSC/plink/plink_omitRef_ukb43760_globularity_XCI2_fullAutosome.sumstats.gz
# details for LDSC
resource_dir=/data/workspaces/lag/shared_spaces/Resource_DB
ldscores_dir=${resource_dir}/LDscores



echo 'Run LDSC heritability'
ldsc.py \
--h2 $glob_munged \
--ref-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
--w-ld-chr ${ldscores_dir}/eur_w_ld_chr/ \
--out ${out_path}/glob_h2_test2